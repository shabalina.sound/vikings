﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class HealthCheck : MonoBehaviour

{
    public vThirdPersonController tpController;
    [FMODUnity.EventRef] public string HealthSnapshot;

    FMOD.Studio.EventInstance SnapInstance;
    public FMOD.Studio.PLAYBACK_STATE state;


    void Start()

    {
        tpController = GetComponent<vThirdPersonController>();
        SnapInstance = FMODUnity.RuntimeManager.CreateInstance(HealthSnapshot);
    }


    void Update()
    {
        Debug.Log(tpController.currentHealth);
        SnapInstance.getPlaybackState(out state);
        if (tpController.currentHealth <= 30)

        {
            if (state != FMOD.Studio.PLAYBACK_STATE.PLAYING)

            {
                SnapInstance.start();
            }


        }

        else if (tpController.currentHealth > 30)

        {
            if (state == FMOD.Studio.PLAYBACK_STATE.PLAYING)

            {
                SnapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }

        }

    }



}
