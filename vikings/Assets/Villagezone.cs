﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;


public class Villagezone : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string villageEvent;

    [FMODUnity.EventRef]
    public string forestEvent;

    FMOD.Studio.EventInstance villageInstance;
    FMOD.Studio.EventInstance forestInstance;
    public GameObject cameraObject;



    // Start is called before the first frame update
    void Start()
    {
        villageInstance = FMODUnity.RuntimeManager.CreateInstance(villageEvent);
        forestInstance = FMODUnity.RuntimeManager.CreateInstance(forestEvent);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(cameraObject.transform.position.y);
        villageInstance.setParameterByName("CameraHight", cameraObject.transform.position.y);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            villageInstance.start();
            forestInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            villageInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            forestInstance.start();

        }
    }
}
