﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class MusicSystem : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string musicEvent;
    FMOD.Studio.EventInstance musicInstance;

    public vControlAIMelee Enemy;
    public bool inCombat = false;


    // Start is called before the first frame update
    void Start()
    {
        musicInstance = FMODUnity.RuntimeManager.CreateInstance(musicEvent);
        musicInstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inCombat && Enemy.isInCombat)

        {
            musicInstance.setParameterByName("GameState", 1f);
            Debug.Log("Combat");
            inCombat = true;
        }
        else if (inCombat && Enemy.isInCombat == false)

        {
            musicInstance.setParameterByName("GameState", 0f);
            Debug.Log("Exploration");
            inCombat = false;
        }
           
    }
}
