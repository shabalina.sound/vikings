﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class fmodfootsteps : MonoBehaviour
{

    [FMODUnity.EventRef] public string footstepsEvent;

    private FMOD.Studio.EventInstance footstepsInstance;
    public vThirdPersonInput tpInput;
    public vThirdPersonController Controller;
    public LayerMask lm;
    private float surfaceType;
    private float locomotionType;

    // Start is called before the first frame update
    void Start()

    {
        tpInput = GetComponent<vThirdPersonInput>();
        Controller = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep()

    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            SurfaceCheck();
            footstepsInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsEvent);
            footstepsInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject.transform.position));
            if (Controller.isSprinting) locomotionType = 1f;
            else locomotionType = 0f;

            footstepsInstance.setParameterByName("locomotion_type", locomotionType);
            footstepsInstance.setParameterByName("Surface_type", surfaceType);
            footstepsInstance.start();
            footstepsInstance.release();


        }
        

    }

    void SurfaceCheck ()

    {
        RaycastHit rh;
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out rh, 0.3f, lm)) 

        {
            Debug.Log(rh.collider.tag);
            if (rh.collider.tag == "Snow") surfaceType = 0f;
            else if (rh.collider.tag == "Water") surfaceType = 1f;
            else if (rh.collider.tag == "WoodSnow") surfaceType = 2f;
            else if (rh.collider.tag == "NearWater") surfaceType = 3f;
            else surfaceType = 0f;
        }

    }

}
