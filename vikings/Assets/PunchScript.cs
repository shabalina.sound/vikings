﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class PunchScript : MonoBehaviour
{


    [FMODUnity.EventRef]
    public string punchEvent;
    public vThirdPersonInput tpInput;

    FMOD.Studio.EventInstance punchInstance;

    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void punch()

    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(punchEvent, gameObject);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(punchInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
            punchInstance.start();
            punchInstance.release();
        }
    }
}
