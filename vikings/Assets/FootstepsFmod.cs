﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class FootstepsFmod : MonoBehaviour

{

    [FMODUnity.EventRef] public string FootstepEvent;
    public vThirdPersonInput tpInput;
    public vThirdPersonController tpController;

    // Start is called before the first frame update
    void Start()

    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footstep ()

    {
        if (tpInput.cc.inputMagnitude > 0.1)

        {

            if (tpController.isSprinting)

            {
                FMODUnity.RuntimeManager.PlayOneShotAttached(FootstepEvent, gameObject);
            }
            else FMODUnity.RuntimeManager.PlayOneShotAttached(FootstepEvent, gameObject);
        }
        
    }


}
